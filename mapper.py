#!/usr/bin/env python

import sys
import re
for line in sys.stdin:
    try:
        article_id, text = line.strip().split('\t', 1)
    except ValueError as e:
        continue
    words = text.split(' ')
    for word in words:
        words = re.findall("\w+", text)
        print "%s\t%d" % (word, 1)
